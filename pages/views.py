from django.shortcuts import render
from django.http  import HttpResponse
# Create your views here.

def home_view(request, *args, **kwargs):
    print(request)
    print(request.user)
    return render(request, 'pages/home.html')

def about_view(request, *args, **kwargs):
    some_context = {
        'my_text' : "this page is about me",
        'number_' : 234,
        'title' : "About ME",
        'my_list': ["sth", 23 , "sth2", 233],
        'my_html': "<h4>Helloooo</h4>",
        }
    return render(request, 'pages/about.html',some_context)