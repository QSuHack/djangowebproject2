
from .views import home_view, about_view
from django.urls import path
urlpatterns = [
    # Uncomment the next line to enable the admin:

    path('', home_view),
    path('about', about_view)
]
